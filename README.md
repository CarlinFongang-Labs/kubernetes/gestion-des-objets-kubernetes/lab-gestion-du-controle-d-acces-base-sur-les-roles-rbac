# Lab : Gestion du Contrôle d'Accès Basé sur les Rôles dans Kubernetes


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectif 
Le contrôle d'accès basé sur les rôles est un élément important lorsqu'il s'agit de gérer un cluster Kubernetes en toute sécurité. Plus il y a d’utilisateurs et de processus automatisés qui doivent s’interfacer avec l’API Kubernetes, plus le contrôle des accès devient important.
Ce lab vise à démontrer le principe d'attribution des droits d'accès RBAC sous Kubernetes afin de limiter de manière appropriée l'accès des utilisateurs, pour cela nous allons : 

- Créer un rôle pour un utilisateur

- Liez le rôle à l'utilisateur crée et vérifiez que la configuration fonctionne


# Contexte

Vous travaillez pour BeeHive, une entreprise qui assure des expéditions régulières d'abeilles aux clients. L'entreprise est en train de créer une infrastructure basée sur Kubernetes pour certains de ses logiciels.

Vos développeurs vous demandent fréquemment de fournir des informations provenant du cluster Kubernetes. Vous souhaitez donc leur donner la possibilité de lire les données du cluster sans y apporter de modifications. À l’aide du contrôle d’accès basé sur les rôles Kubernetes, assurez-vous que l’ devutilisateur peut lire les métadonnées du pod et les journaux de conteneur à partir de n’importe quel pod de l’ beebox-mobileespace de noms.

Un fichier kubeconfig pour l' utilisateur **"dev"** a déjà été créé sur le serveur. Vous pouvez utiliser ce fichier pour tester votre configuration RBAC  :

```bash
kubectl get pods -n beebox-mobile --kubeconfig /home/cloud_user/dev-k8s-config
```

Une commande utile facultative est la suivante : `kubectl auth can-i` peut être utilisée pour vérifier si un utilisateur dispose des autorisations nécessaires pour effectuer une action spécifique sur une ressource spécifique dans un cluster Kubernetes. Par exemmple : `kubectl auth can-i create pods`.


# 1. Création d'un role pour l'utilisateur "dev"

1. Connexion au control node

On va commencer par se connecter au cluster

```bash
ssh -i id_rsa user@public_ip_address
```

2. Test de l'accès avec le role de **"dev"**

Nous allons ensuite essayé de faire passer la commande suivante et observer le résultat

Une fois connecter au control node, créer le fichier **dev-k8s-config** 

```bash
nano dev-k8s-config
```
Contenu : 

```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMvakNDQWVhZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJME1EVXlNakEyTkRRME5Gb1hEVE0wTURVeU1EQTJORFEwTkZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTFphClE4UUQyODN2TmQwdFNNQ09kMkRrVSt6OVBuZk85V2JCcm1XVHhWeCtlZHpsM2ltbk1pSVRuVUtwMXQ2US9YWEIKZWV6czhwUDRxRE1CUTJhdTBJTWdiSGFIR3ZsaGExRWJFR09KdTJwKzZOVitEZUZtZlBjVEV1a1pSbVQxdDFNNApyNmhRenJ3Y2t5dzZXL3RWRE1lT2tlQm83YlhnMURUZVNhVkdnRTAxejRkam8vOGU5T1BqYkVtY0JxRFFKYjhHCmtlem1ncnA4T2FLdUhtZnJmdm5qNTRmZDdFWGZaQWlTUVh0cVY4TG5RWWJYOTBDeVhJbExuT1V0eXBzQkpaVG0KcWtsenZlVG1TY2JOS3NHUU5uTGkxTkRUdjBpSFJGbTlvNHNVUUlXaE9NOUhveUVNZ2UvbUNZNFp1UVhWaXU5QgpaMEVDOWNJczdzZDZyenJvRGZjQ0F3RUFBYU5aTUZjd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZLSHcycVZKNWgzVlY5cDVJd20zYmNIbEJTQVVNQlVHQTFVZEVRUU8KTUF5Q0NtdDFZbVZ5Ym1WMFpYTXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBRkZFZnRBM2IyY05pMlNTY09MVQpXUEMzS25MQWtsaWxsTVJoSURaU2pRVTRkczIybW1pOFJ5VWZLQ2xmWitPMlhnY2FWL1JpejVML1dMNHYvRnpsClJaWU4vV1VHdkxmeU91Mm14L0cvZkw5YlVCaDFKUkFhS1VXUHpjYzA1aHNzNzRNWWMwNkJBNkhUTkp3MlhkbDgKVmNONlVIblpDNGJoZWR5dUc1VlM2Wnc2TFhYVEVuRzBDWWVqV1JpMVRDOFUyZXdZQWJJT3hqWHFnSkVkVEI2NApwU3czNmcwanJScGliajJPcVBHeXJXYXlBV05waDBiREVoRW96UWJEa0JqMjZQbmIzLzBJM1NtbGlYWFpKZTVVCml1TlRVRFB0d09pc0ZNM29MZFdRa1dXMVdlejYvc2lyeForTGpkZTBESml6dm52bUg5TEsxT01zOVV2VTdtZFkKVXNrPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://10.0.1.101:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: dev
  name: dev
current-context: dev
kind: Config
preferences: {}
users:
- name: dev
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUVBakNDQXVxZ0F3SUJBZ0lSQVBkbG1zMGoxdnI3dnZVS29hZ0hiL0V3RFFZSktvWklodmNOQVFFTEJRQXcKRlRFVE1CRUdBMVVFQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TkRBMU1qSXdOalF4TUROYUZ3MHlOVEExTWpJdwpOalF4TUROYU1Cd3hEREFLQmdOVkJBb1RBMlJsZGpFTU1Bb0dBMVVFQXhNRFpHVjJNSUlDSWpBTkJna3Foa2lHCjl3MEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUFwME5XVXVleFFDeXNvQnpNYXJvZkhoOEh3WENVd2FObnF4bWsKMkZqR1NhcUo5Z29zelZzNDVuTzBxNS92dVJmTERESS9YOEVWMjhFWGlMZ3MvL1VHMkJESTRzVWJwZXc1MlM1bQp4S3VGeXRsQ003NW5vcFBBcDhhN1k5MlNybzZCZWVnV1VmVWQrR3k5bzkwb2dUa3NYYVhPT1FOT1ppQ2hIV1ZxCjdEd0N5RlROWnhmVUVjN0VnOTZheFA2aEZubE52YkVST05vSHpPeG52cWV5THk4ek1YUVkra0daU1F6NmFFbEsKYkVxbVJZWFlXbWYyTEVGamNkRlV5WktlSm9sdnVSWXlvWVJBQ3dFeGNtYVkwamxNYVFORW1hbU53L3ZzQ2JkQQp1NDhIRkJ0aGJSOHJvZTRWQTR2RndISktDQ1l0OHV6UDN5V1ZSeUN5NFlhWHdZbkdseUp2RC9Hc1NBSXhOQVB4CkR0Z0pTaEJCZGsxU3d0TzdnSWw2aVdOeUI4b2lSNlRnUnMza0tYWXdDc3FVRSsyZ0dJWGZsektLRFJPZzZqRmQKVUhlNDZYR3h4TmhidDhCUHEyV1BXdm9CM3RPcmFQaHpxRXlFbXFNdzk5dktQMitldU5XYlNSNjJkWXFnZERVNgo5cEJRdU5IWGpGVVFVbTBOR1lldmVnazYxZTJ5b2d3RkM3RDhLV1k3RWpNL3BEZFBmOHpZamw2SHJYOGJJTmVuCnAzOFVIbWtVYVE5UXRsOW5ndzJOTUJxSzRQcVk0RUcxR0p4aDFzNEN1dWZUZ1lLNnh3cllxdFN2aFdPTTlwQlkKanZRUEdhN214TlAwTnY4Qjlud1JpRWJHZVVsSHlrUllmSU45WHJUTi9maEpIRU9RY3JjYU5RMkJsNkdNelkwbwpFVlUzL2c4Q0F3RUFBYU5HTUVRd0V3WURWUjBsQkF3d0NnWUlLd1lCQlFVSEF3SXdEQVlEVlIwVEFRSC9CQUl3CkFEQWZCZ05WSFNNRUdEQVdnQlNoOE5xbFNlWWQxVmZhZVNNSnQyM0I1UVVnRkRBTkJna3Foa2lHOXcwQkFRc0YKQUFPQ0FRRUF0Qi9hK2pSL0dtNWsxTkpLOTNMak5xcmVpOTlKR2ZyaXVLMGZrYy9pY0VkYU8vNWU3M3E4WU1kZApaZHV4bE84RTlnVVJQWkxqS3BKekk2MERiL2NySlRhdGhJQmh5OThLQ3ExcXFqWTlVYUs1ZlUvb2RqSzY5SWlzCnY4Ti85eUNLbHB3Q082eFphY3dXdWI5QWtmNXlEOUNkNHZjeVpaVTEwN3FlNlk0b01sUTAwQ1lRSmZYeHVHUUsKRTcxY1YwbmdYT0U2Z09FMHJob0Frb2VURnNwbG5laEhrMEYxdHp4c0k2SURVQTR6R1dDeXBJV2NkQWtjcE91VQovbmNVaVVxWjhpTkVSRTVGaEExNGNLU2JiMXhHT3JnSzVHTFdPSHBDNFlWcStNNWxad29lN0pYRFFQdUZtUFowCllyeDI5alBFQlhmM0Z2UjY0UysyRDFoMmRTNStGQT09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
    client-key-data: LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUpRd0lCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQ1Mwd2dna3BBZ0VBQW9JQ0FRQ25RMVpTNTdGQUxLeWcKSE14cXVoOGVId2ZCY0pUQm8yZXJHYVRZV01aSnFvbjJDaXpOV3pqbWM3U3JuKys1RjhzTU1qOWZ3Ulhid1JlSQp1Q3ovOVFiWUVNaml4UnVsN0RuWkxtYkVxNFhLMlVJenZtZWlrOENueHJ0ajNaS3Vqb0Y1NkJaUjlSMzRiTDJqCjNTaUJPU3hkcGM0NUEwNW1JS0VkWldyc1BBTElWTTFuRjlRUnpzU0QzcHJFL3FFV2VVMjlzUkU0MmdmTTdHZSsKcDdJdkx6TXhkQmo2UVpsSkRQcG9TVXBzU3FaRmhkaGFaL1lzUVdOeDBWVEprcDRtaVcrNUZqS2hoRUFMQVRGeQpacGpTT1V4cEEwU1pxWTNEKyt3SnQwQzdqd2NVRzJGdEh5dWg3aFVEaThYQWNrb0lKaTN5N00vZkpaVkhJTExoCmhwZkJpY2FYSW04UDhheElBakUwQS9FTzJBbEtFRUYyVFZMQzA3dUFpWHFKWTNJSHlpSkhwT0JHemVRcGRqQUsKeXBRVDdhQVloZCtYTW9vTkU2RHFNVjFRZDdqcGNiSEUyRnUzd0UrclpZOWErZ0hlMDZ0bytIT29USVNhb3pEMwoyOG8vYjU2NDFadEpIcloxaXFCME5UcjJrRkM0MGRlTVZSQlNiUTBaaDY5NkNUclY3YktpREFVTHNQd3BaanNTCk16K2tOMDkvek5pT1hvZXRmeHNnMTZlbmZ4UWVhUlJwRDFDMlgyZUREWTB3R29yZytwamdRYlVZbkdIV3pnSzYKNTlPQmdyckhDdGlxMUsrRlk0ejJrRmlPOUE4WnJ1YkUwL1EyL3dIMmZCR0lSc1o1U1VmS1JGaDhnMzFldE0zOQorRWtjUTVCeXR4bzFEWUdYb1l6TmpTZ1JWVGYrRHdJREFRQUJBb0lDQUg1REZxYit2NDd5S2wyelg1d3hZOHdaCklJR2x1cVZ4OUkvMGk3dkZSK25VRXREb1hXbW5zd3hEeTk3Q25QUGNRaXF5dzZSRTNYdEdCNzN0U2dydHJIenUKTHJjQWxCNXNkRDYrQ0ZYdUpORTdyLzNmZmR2OExUSjVrbnVRaDJtOXFYK2M4enhyREhTN29XeFJSb0pGNW5mSwo4aUdYblZiNnlxU21LbENwL0crOWxGQitiRmdFL2RPTjlzYytiOXZITmlnN3dnUm9rZnJmZW5FeHBoQjRVT3pECjl4Ynk3Z04zQ0pBd3d6MThISWZld2JOOVJ4ZWhSTEJtbE5xVkUwMkQ4aEtGN3pKOTR0cHJ6Q2ZQeXd6bXRBZVIKci9kUFJ2VHE5d05la2o4RGVSUjBsdGNJOEtjTVVsQ2E4bXlUMmM3TkEyS2Q0VTFSMFo0ZjBPanQ4U0RvSUVBOQo4RnlSRlVlV1BqUHJjOUx5ZmgvV2doQXF3WFhUcjdlaExZRDZTNkNDUnBhaFVyaU9NWE9Yb3pQQTQ5eU9TNjV5CkFPZHRubkhLdXlmdEtuVHU2Smlyc0E5aFZ2R3VuZGZHZS91ekZlL3NMWnJzZndqTHlkYXFEUVdKYXBWemxReTUKQmtKaTFVeExCcWdBV2hDYmpZcHNsNis5ZWxORytlQVRRdDZtOWFjbXQ4QXRpZEtTK2xzSVFQdzh4dDRBaElXUQo3ZENXdnJvZnZDWEJJeVFnZ2hHYyt3aW0zWVQ3cE5XLzFqUm1sQWlGbFBOVFJZU1h5Uy9tRVp3bjlqZzB3UHRBCnJNTmNtazY0eVF3WGY1aGxqMG0rcFcrdm00cTJjUTA4cUdNQm9IWEZJNHMrQmRwMGthcXRkWWxOSVRyREdGbEkKaUVuZ21ZL2l1SnY1RFc1SVJxSUJBb0lCQVFEVk9aTzdwbHF0SWtqWkpJUkV0R0pDZXBsRnZzUEJ3cFIzR1ZtMQp1TEp0QWpadVl1ZG1iS3AyMlFaMzJ2YU16MzZqUHdsN0wza3c3eEZSR2R6ZUx1TzBGUC85WGQ3UHdqblc0clZrCm1wdkxEdjlFanppa0xlM0tQQ2NvT2tWWDNWNHJ3Zi9nTHdCQThqTzdqTkdHZ2pKeDkyNGZWaVBXbWV0RUYxcVcKa0R4SURiUzRGWHVEMWNKNkV3VnNSMGpESkJFaDBpdHJ6OThXWlZuSzNpODNySGl0UGlURzNFekUyV3M0a3RkSgp1SWF6Ti9hS1pRSlpxellwRjVUU2Q3UktUTnFGLzF0a0UrbTJiSWl1aGZWZHJoVVI0UWFPcWhtSnlZTmRGMVhqCnkveFpEbkJxWnkyWXdjNTFiVEdVYUhXRzZRUGJocnBoR0FZUXNFNWJDWG9DU29uVEFvSUJBUURJMFZVZjZGTWMKRU5wa2pLSGtEUEgycXRncFJGaGc3ZTlMei9ReEl0Y2RBWGhWYUFzZFpacHdKMFMzUjE1Y1RTUC9ocGNVWWwwSApPUWxadGR6SWFkdm9TZ0FMenJqSlpLejdNc01sWDh0M3U5YUdZd0JUZGZWd0hxRGxVamZkNlJnR0ppU2FKSVlDCnRlK2h2ZlFUNnM5RGpJUEZrbHFncGZyZEZHTWZiNVBleHlzUXA3SjRRcVVybVBWR2RSS09mRFZEYm9pdHdQM3cKMzg4REl3YXFqM1hpTUNiMnlhRWNmY2FBOW04RmIyc0pTOEVhcy9xZkNtQW11M0tlWHZKcnpBK2RldWRtMmFVdQpmblZVcmtENGlJTzJuREJxL0ZGY0VxMHI4bXA0SitkaUJKdnB3Q3JZODdPd0pKSnJteEF4eXJqZkM1bnNlalV2CnlFMURJRi9ucnZsVkFvSUJBSDRNN3FmMVBRK3kvK3ZXYVpjUUtESnl0WitFbWNjMjEzdmM4YXE3Q0VmbkVmU0oKNVpBb1YxR3hGT0RRQWdiV04yQWNTT2VZOWdVV24xOFpjZmVyTXBOb0FybHV2K1NFUnBzaEpPalhlWmcrUW12dQpXeWE3dkNKZEV5ejEzYjZHc0VBZ0RMc2tMZnMyODV4Qk1EQmJVYVVNQi83Z2RvbE1Mc2Z2SW05MXp4MHRkUk9LCko1MlZSY1hTN3RiSEFWM1RPWW1MN0gxc3dzNURsaVNXMnNVSDRjTlF4SDhIQng4c0JQWEo0Um5FeTJTeGhiN2sKM2RIU3BFOEFrTXNuVnQ3QnRrR1c1eGt2cUc5ZWxDTFFNSEIwRkNLZ3E3YTFHaW5aaGg0SFNKQkZLK1lkNGhrVQo2K0N0V2ZjVFdVY0p4Y2krZ2JoS1dtNUJ2SHdyTGZjbGtBcEF0RjhDZ2dFQkFNT0lMOTUxbEJiTHdob05aTVJzClpsekxWTlk1bWVRblNNMEVtcndOTkc4bHMwbFZtUnp5aFVHV3VxM0thL0pybWhWL3N6TkI0MXJaQ21XelVscEIKMk1HQTdES1RBaEgyV1FmdUVWMGV2VHJFUnNsck5ESVB0UCtLa3hjMnl2YmpzMlNwVnhKUFpielY4WklISnRVTgoyT25vczVWNFA0NFVqd01jaXJkZEtSbk55L3pYakVyVWVZYk4xR1RhTVV0VFhYbGU4dTViNytiTnhQOUU1NFREClV3RDk2Yko1K01tSkhmTU1BR1hHYXU0T09PUUxRb2dBemtab1ZkNG1qTnl4UUxNUnJLYlVYS1c4cy9uVUFhYjkKSldKMUNoL2h0a1owV1BGbGhJMmlMUVdmZjBCR3dIR21PTnZWZ0V3T0crS2ZoUXFJZlVOb2Fza3Z1eXNWSnN5cgo3bEVDZ2dFQkFORUNvc1F3ZitvMEFJMVV0WHlselpTZ1dPTzBiaW5KckZsTzVDaVRxbGlMdVRrcVZ3dEY4dG1VCk1qQXFQbWFROVUyZk42UldtcEI2WHVqUDdpOG9KTldTdHpUY0I5ZHJ5NDJHRitMRW5QMTUvZ0xaSWpzU0dtM0cKZjh1R3JCc1g2YjlnV243WktYQjJVTlk4Zlc2REQwR3kxZTFLUHdwQUc1VHZjbWtzb2NxR0hBdzk2SlNRRm00MAppdVcrSjRPL2tFaTE1ZGNLZ2hsdlc4MzNxWVhPYmhmenZlQ0hzaVVGbmFBdWJzUVAra2k3WU5rSk13c3lXaE5WClVVMXlJWm9vYWhNVzNPbytxVDVRL0NNNklIVHpMYVE1bDlicjBOc0J5b1JieDIyRVNCZUh1ZC9iT0pna3YvRkMKTjBCRE1xZWp3djRsZkRCaXlnczJ4OWQrQ3VVU0MzTT0KLS0tLS1FTkQgUFJJVkFURSBLRVktLS0tLQo=
```

Nous allons essayer ensuite de liste les pods en nous servant de cette configuration dédié aux dev

```bash
kubectl get pods -n beebox-mobile --kubeconfig dev-k8s-config
```

Cette commande permet de lister les pods dans le namespace beebox-mobile d'un cluster Kubernetes en utilisant un fichier de configuration spécifique (dev-k8s-config).

>![Alt text](img/image.png)
*Nous n'obtenons pas le resustat escompté pour l'instant*


3. Fichier de spécification de role


Pour mettre en oeuvre notre control d'acces, nous allons créer un fichier de spécification pour le nouveau role que nous utiliserons pour les developpeurs.

```bash
nano pod-reader-role.yml
```

contenu 

```bash
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: beebox-mobile
  name: pod-reader
rules:
- apiGroups: [""]
  resources: ["pods", "pods/log"]
  verbs: ["get", "watch", "list"]
```

4. Création du role

```bash
kubectl apply -f pod-reader-role.yml
```

>![Alt text](img/image-1.png)
*Application du role*

A présent que le role à été crée, afin que les developpeurs puissent l'utiliser, nous allons mettre en place un Rolebinding afin de créer une liaison.

# 2. Définir un RoleBinding qui lie un utilisateur nommé `dev` au rôle `pod-reader`.

   1. Créer un fichier nommé `rolebinding.yaml`.

```bash
nano pod-reader-rolebinding.yml
```

```bash
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: pod-reader
  namespace: beebox-mobile
subjects:
- kind: User
  name: dev
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```


   2. Création du RoleBinding avec la commande `kubectl apply -f rolebinding.yaml`.

Créez le RoleBinding :

```bash
kubectl apply -f pod-reader-rolebinding.yml
```
>![Alt text](img/image-2.png)


  3. Testez à nouveau l'accès pour vérifier que vous parvenez à répertorier les pods :

```bash
kubectl get pods -n beebox-mobile --kubeconfig dev-k8s-config
```

>![Alt text](img/image-3.png)
*Nouveau role crée*

4. Vérifiez que l'utilisateur "dev" peut lire les journaux du pod :

```bash
kubectl logs beebox-auth -n beebox-mobile --kubeconfig dev-k8s-config
```

>![Alt text](img/image-4.png)
*Traitement de l'auth en cours*


5. Vérifiez que l'utilisateur "dev" ne peut pas apporter de modifications en tentant de supprimer un pod

```bash
kubectl delete pod beebox-auth -n beebox-mobile --kubeconfig dev-k8s-config
```

>![Alt text](img/image-5.png)
*Pas d'autorisation pour la suppression du pod*

L'on peut constater que malgré l'accès aux informations du pod tels que la lecture des logs, nous n'avons pas l'autorisation pour modifier ce pod.


# Reference 

https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands